Source: python-designateclient
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Corey Bryant <corey.bryant@canonical.com>,
 David Della Vecchia <ddv@canonical.com>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-cliff,
 python3-coverage,
 python3-debtcollector,
 python3-hacking,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-mock,
 python3-openstackdocstheme,
 python3-osc-lib,
 python3-oslo.config,
 python3-oslo.utils,
 python3-oslotest,
 python3-requests,
 python3-requests-mock,
 python3-sphinxcontrib.apidoc,
 python3-stestr,
 python3-stevedore,
 python3-subunit,
 python3-tempest,
 subunit,
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/openstack-team/clients/python-designateclient.git
Vcs-Browser: https://salsa.debian.org/openstack-team/clients/python-designateclient
Homepage: https://github.com/openstack/python-designateclient

Package: python-designateclient-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: client library for the OpenStack Designate API - doc
 Designate provides DNSaaS services for OpenStack. It provides a multi-tenant
 REST API for domain & record management. It is Integrated with Keystone for
 authentication, and provides a framework in place to integrate with Nova and
 Neutron notifications (for auto-generated records). Designate supports
 PowerDNS and Bind9 out of the box.
 .
 This is a client for the OpenStack Designate API. There's a Python API
 (the "designateclient" module), and a command-line script ("designate").
 .
 Installing this package gets you a shell command, that you can use to
 interact with Designate's API.
 .
 This package provides the documentation.

Package: python3-designateclient
Architecture: all
Depends:
 python3-cliff,
 python3-debtcollector,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-osc-lib,
 python3-oslo.utils,
 python3-pbr,
 python3-requests,
 python3-stevedore,
 ${misc:Depends},
 ${python3:Depends},
Description: client library for the OpenStack Designate API - Python 3.x
 Designate provides DNSaaS services for OpenStack. It provides a multi-tenant
 REST API for domain & record management. It is Integrated with Keystone for
 authentication, and provides a framework in place to integrate with Nova and
 Neutron notifications (for auto-generated records). Designate supports
 PowerDNS and Bind9 out of the box.
 .
 This is a client for the OpenStack Designate API. There's a Python API
 (the "designateclient" module), and a command-line script ("designate").
 .
 Installing this package gets you a shell command, that you can use to
 interact with Designate's API.
 .
 This package provides the Python 3.x module.
